def largest_divisor(number):
    count = 1
    while True:
        count += 1
        if number % count == 0:
            return count


number = int(input('Введите число больше 1: '))

print(f'Наименьший делитель, отличный от единицы: {largest_divisor(number)}')