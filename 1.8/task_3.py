def sum_all_digits_number(number):
    last_num = 0
    summ_digits = 0
    while number != 0:
        last_num = number % 10
        summ_digits += last_num
        number //= 10
    return summ_digits


def digits_in_number(number):
    count = 0
    while number != 0:
        number //= 10
        count += 1
    return count


number = int(input("Введите число: "))
print(f'Сумма чисел: {sum_all_digits_number(number)}')
print(f'Количество цифр в числе: {digits_in_number(number)}')
print(f'Разность суммы и количества цифр: {sum_all_digits_number(number) - digits_in_number(number)}')