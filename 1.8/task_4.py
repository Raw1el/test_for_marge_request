

def number_back(number):
    parts = str(number).split('.')
    parts[0] = ''.join(reversed(list(str(parts[0]))))
    parts[1] = ''.join(reversed(list(str(parts[1]))))
    return float(parts[0] + '.' + parts[1])


first_num = float(input('Введите первое число: '))
second_num = float(input('Введите второе число: '))

first_num_back = number_back(first_num)
second_num_back = number_back(second_num)

print(f'Первое число наоборот: {first_num_back}')
print(f'Второе число наоборот: {second_num_back}')
print(f'Сумма: {first_num_back + second_num_back}')
